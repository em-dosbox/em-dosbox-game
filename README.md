# em-dosbox-game

## troubleshooting
docker run --rm -it --entrypoint bash <image>

## Dockerfile ARGs
Use these with: ex.

```bash
docker build -t doom --build-arg game=doom --build-arg exe=DOOM.EXE .
```

| Game | Short Name | ARG game | ARG exe | Genre |
| --- | --- | --- | --- | --- |
| Doom | doom | doom | DOOM.EXE | FPS |
| Commander Keen 6 | keen6 | keen6 | keen6.exe | Action |
| Lode Runner | lodetlr | lodetlr | LR.BAT | Action |
| MicroLeague Baseball | mlbb | mlbb | Mlbb.com | Sports |
| Lakers versus Celtics and the NBA Playoffs | nba | nba | nba.exe | Sports |
| Prince of Persia | prince | prince | PRINCE.EXE | Action |
| SimEarth | simearth | simearth | SIMEARTH.EXE | Simulation |
| Space Quest 1 | sq1 | sq1 | SQ.COM | Adventure |
| Where In The World Is Carmen Sandiego? | carmen | carmen | CARMEN.EXE | Adventure |
| The Oregon Trail Deluxe | oregondx | oregondx | OREGON.EXE | Simulation |
| Ultima 6 | ultima6 | ultima6 | ULTIMA6.EXE | RPG |