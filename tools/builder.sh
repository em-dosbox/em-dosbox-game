#!/bin/bash
game=(alien brain bubble candc carmen cvania darkc doom dinoadv dune gadget isaac lodetlr mlbb nba oregondx prince sgate sq1 ultima6 warcraft ys) 
exe=(ALIEN.EXE BRAIN.EXE BUBBLE.EXE cnc_en.com CARMEN.EXE CEGA.EXE DC.EXE DOOM.EXE KAV.EXE DUNE.BAT gadget.exe KAV.EXE LR.BAT Mlbb.com nba.exe OREGON.EXE PRINCE.EXE SGATE.EXE SQ.COM ULTIMA6.EXE war.exe ys.exe)

docker rmi -f $(docker images -q)

for (( g=0,e=0; g<${#game[@]},e<${#exe[@]}; g++,e++))
do
  echo "Starting: game=${game[$g]} exe=${exe[$e]}"
  docker build --quiet --no-cache --build-arg game=${game[$g]} --build-arg exe=${exe[$e]} -t dcassiero/em-dosbox-${game[$g]}:gamething-v1.0 . && docker push dcassiero/em-dosbox-${game[$g]}:gamething-v1.0
  echo "Finished: game=${game[$g]} exe=${exe[$e]}"
done
