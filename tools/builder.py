#!/usr/bin/env python

import argparse
import subprocess
import logging

from yaml import safe_load

"""
 This script reads a list of games and builds/pushes a docker image to a registry.
 Assumes logged in to registry
"""

# TODO: log in to registy. might want args for dockerfile, registry, build only...

__AUTHOR__ = 'Daniel Cassiero <daniel.cassiero@gmail.com>'
__LICENSE__ = 'MIT'

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M', level=logging.INFO)

def get_constants(path):

    data = None

    with open(path + 'dosgames.yaml', 'r') as stream:
        try:
            data = safe_load(stream)
        except FileNotFoundError:
            logging.error("load error on {}".format(stream))

    return data.get('game'), \
           data.get('exe')

def parseargs():

    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', type=str,
                        required=True,
                        help='Path to list of games (dosgames.yaml)')
    parser.add_argument('-t', '--tag', type=str,
                        required=True,
                        help='Docker tag.')

    return parser.parse_args()

def docker_build(game, exe, tag):
    image_list = []

    for g, e in zip(game, exe):
        cmd = "docker"
        build_args = "build --quiet --no-cache --build-arg game=" + g + " --build-arg exe=" + e + " -t dcassiero/em-dosbox-" + g + ":" + tag + " ."
        image = "dcassiero/em-dosbox-" + g + ":" + tag
        image_list.append(image)
        #print(cmd, build_args)
        #subprocess.run([cmd, build_args])
        subprocess.run(["docker", build_args])

    return image_list

def docker_push(image_list):
    
    for image in image_list:
        cmd = "docker"
        push_args = "push " + image
        #print(cmd, push_args)
        #subprocess.run([cmd, push_args])
        subprocess.run(["docker", push_args])

def main():
    args = parseargs()

    game, exe = get_constants(path=args.path)
    tag = args.tag
    docker_push(docker_build(game, exe, tag))

if __name__ == '__main__':
    main()
