FROM dcassiero/em-dosbox-base:gamething-v1.0
LABEL maintainer="Daniel Cassiero <daniel.cassiero@gmail.com>"

ARG game
ARG exe

WORKDIR /src/em-dosbox/src
RUN mkdir -p /src/em-dosbox/src/assets
COPY source assets/
RUN wget https://devopsny.com/blog/em_dosbox/${game}.zip && unzip ${game}.zip && ./packager.py index ${game} ${exe}

EXPOSE 8000

ENTRYPOINT ["emrun"]
CMD  ["--no_browser", "--port", "8000", "index.html"]
